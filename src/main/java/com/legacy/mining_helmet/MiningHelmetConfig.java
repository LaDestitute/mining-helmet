package com.legacy.mining_helmet;

import org.apache.commons.lang3.tuple.Pair;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;

@Mod.EventBusSubscriber(modid = MiningHelmetMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class MiningHelmetConfig
{
	public static final ForgeConfigSpec CLIENT_SPEC;
	public static final ForgeConfigSpec SERVER_SPEC;
	public static final ClientConfig CLIENT;
	public static final ServerConfig SERVER;

	public static boolean helmetCoversFace;

	public static int zombieHelmetSpawnChance;

	static
	{
		{
			final Pair<ClientConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = pair.getLeft();
			CLIENT_SPEC = pair.getRight();
		}
		{
			final Pair<ServerConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(ServerConfig::new);
			SERVER = pair.getLeft();
			SERVER_SPEC = pair.getRight();
		}
	}

	private static String translate(String key)
	{
		return new String(MiningHelmetMod.MODID + ".config." + key + ".name");
	}

	@SubscribeEvent
	public static void onLoadConfig(final ModConfig.ModConfigEvent event)
	{
		ModConfig config = event.getConfig();

		if (config.getSpec() == CLIENT_SPEC)
		{
			ConfigBakery.bakeClient(config);
		}
		else if (config.getSpec() == SERVER_SPEC)
		{
			ConfigBakery.bakeServer(config);
		}
	}

	private static class ClientConfig
	{
		public final ForgeConfigSpec.ConfigValue<Boolean> helmetCoversFace;

		public ClientConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Client side changes.").push("client");
			helmetCoversFace = builder.translation(translate("helmetCoversFace")).comment("The Mining Helmet will cover the face of whatever mob is wearing it.").define("helmetCoversFace", true);
			builder.pop();
		}
	}

	private static class ServerConfig
	{
		public final ForgeConfigSpec.ConfigValue<Integer> zombieHelmetSpawnChance;

		public ServerConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Server and Client side changes.").push("common");
			zombieHelmetSpawnChance = builder.translation(translate("zombieHelmetSpawnChance")).comment("The chance for a Zombie to spawn with a Mining Helmet (1 in x, 0 prevents them from spawning with helmets)").define("zombieHelmetSpawnChance", 20);
			builder.pop();
		}
	}

	@SuppressWarnings("unused")
	private static class ConfigBakery
	{
		private static ModConfig clientConfig;
		private static ModConfig serverConfig;

		public static void bakeClient(ModConfig config)
		{
			clientConfig = config;
			helmetCoversFace = CLIENT.helmetCoversFace.get();
		}

		public static void bakeServer(ModConfig config)
		{
			serverConfig = config;
			zombieHelmetSpawnChance = SERVER.zombieHelmetSpawnChance.get();
		}
	}
}
