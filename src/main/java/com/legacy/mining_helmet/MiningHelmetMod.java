package com.legacy.mining_helmet;

import com.legacy.mining_helmet.MiningHelmetEvents;
import com.legacy.mining_helmet.MiningHelmetConfig;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.ExtensionPoint;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.network.FMLNetworkConstants;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(MiningHelmetMod.MODID)
@Mod.EventBusSubscriber(modid = MiningHelmetMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class MiningHelmetMod {
	//Original dynamic light mod credited to Xevinaly (Portable Light Sources)
	//Mining Helmet mod credited to Modding Legacy
	//Ported to 1.16.3 by LaDestitute
	//This update of the mod includes jsons to dynamically add light-sources
	//Meaning that this mod can potentially add support for other mods and/or
	//Include a resource pack that allows end-users to tweak which lights are enabled

	//Todo
	//Implement a means of dynamically setting the light-value to match the json-tag files
	//As currently, light will only generate if AirLightSource is given a static-light value
	//For now, "EVERYTHING" uses a light level of 12

	public static final Logger LOGGER = LogManager.getLogger();
	public static final String MODID = "mining_helmet";


	public MiningHelmetMod() {
		//Config loading
		ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, MiningHelmetConfig.CLIENT_SPEC);
		ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, MiningHelmetConfig.SERVER_SPEC);

		//The event bus subscriber for all events that are non-ore or non-worldgen related
		MinecraftForge.EVENT_BUS.register(new MiningHelmetEvents());
		//Compatibility display test for the mod
		ModLoadingContext.get().registerExtensionPoint(ExtensionPoint.DISPLAYTEST, () -> Pair.of(() -> FMLNetworkConstants.IGNORESERVERONLY, (a, b) -> true));

		//The bus that listens for block/etc registration
		IEventBus modEventBus =  FMLJavaModLoadingContext.get().getModEventBus();
		MiningHelmetRegistry.ITEMS.register(modEventBus);
		MiningHelmetRegistry.BLOCKS.register(modEventBus);
	}

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MiningHelmetMod.MODID, name);
	}

	public static String find(String name)
	{
		return MiningHelmetMod.MODID + ":" + name;
	}
}

