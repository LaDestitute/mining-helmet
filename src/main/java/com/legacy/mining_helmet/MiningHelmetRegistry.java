package com.legacy.mining_helmet;

import com.legacy.mining_helmet.block.AirLightSource;
import com.legacy.mining_helmet.block.WaterLightSource;
import com.legacy.mining_helmet.item.HelmetArmorMaterial;
import com.legacy.mining_helmet.item.MiningHelmetItem;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class MiningHelmetRegistry {
	public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, MiningHelmetMod.MODID);
	public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, MiningHelmetMod.MODID);

	//The mining helmet
	public static final RegistryObject<Item> MINING_HELMET = ITEMS.register("mining_helmet", () ->
			new MiningHelmetItem(HelmetArmorMaterial.MINING,
					EquipmentSlotType.HEAD, new Item.Properties().group(ItemGroup.COMBAT)));

	//These two blocks must have light values set or the system will not work for blocks that emit light
	public static final RegistryObject<Block> AIR_LIGHT_SOURCE = BLOCKS.register("air_light_source",
			() -> new AirLightSource(Block.Properties.create(Material.AIR).setLightLevel(blockState ->
					12).doesNotBlockMovement().noDrops()));

	public static final RegistryObject<Block> WATER_LIGHT_SOURCE = BLOCKS.register("water_light_source",
			() -> new WaterLightSource(Block.Properties.create(Material.OCEAN_PLANT).setLightLevel(blockState ->
					12).doesNotBlockMovement().noDrops()));
}
