package com.legacy.mining_helmet.client;

import com.google.common.collect.ImmutableList;
import com.legacy.mining_helmet.MiningHelmetConfig;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.LivingEntity;

public class TorchHelmetModel<T extends LivingEntity> extends BipedModel<T>
{
	public ModelRenderer Base;
	public ModelRenderer Lid;
	public ModelRenderer Light;

	public TorchHelmetModel()
	{
		super(1.0F);

		this.textureWidth = 64;
		this.textureHeight = 64;

		float offsetAmount = !MiningHelmetConfig.helmetCoversFace ? 2 : 1;
		this.Lid = new ModelRenderer(this, 0, 16);
		this.Lid.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.Lid.addBox(-6.0F, -4.0F - offsetAmount, -6.0F, 12, 2, 12, 0.0F);
		this.Base = new ModelRenderer(this, 0, 0);
		this.Base.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.Base.addBox(-4.0F, -8.0F - offsetAmount, -4.0F, 8, 8, 8, 1.0F);
		this.Light = new ModelRenderer(this, 32, 0);
		this.Light.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.Light.addBox(-3.0F, -10.0F - offsetAmount, -7.0F, 6, 6, 3, 0.0F);
	}

	@Override
	protected Iterable<ModelRenderer> getHeadParts()
	{
		// TODO SUPER HACKY FIX UNTIL FORGE CAN FIX setRotationAngles
		float offset = this.bipedHead.rotationPointY;

		this.Lid.copyModelAngles(this.bipedHead);
		this.Base.copyModelAngles(this.bipedHead);
		this.Light.copyModelAngles(this.bipedHead);

		this.Lid.rotationPointY = offset;
		this.Base.rotationPointY = offset;
		this.Light.rotationPointY = offset;

		return ImmutableList.of(this.Lid, this.Base, this.Light);
	}

	@Override
	protected Iterable<ModelRenderer> getBodyParts()
	{
		return ImmutableList.of();
	}

	@Override
	public void setRotationAngles(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		super.setRotationAngles(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
		/*float offset = this.bipedHead.rotationPointY;
		
		this.Lid.copyModelAngles(this.bipedHead);
		this.Base.copyModelAngles(this.bipedHead);
		this.Light.copyModelAngles(this.bipedHead);
		
		this.Lid.rotationPointY = offset;
		this.Base.rotationPointY = offset;
		this.Light.rotationPointY = offset;*/
	}

	public void setRotateAngle(ModelRenderer ModelRenderer, float x, float y, float z)
	{
		ModelRenderer.rotateAngleX = x;
		ModelRenderer.rotateAngleY = y;
		ModelRenderer.rotateAngleZ = z;
	}
}
